package com.good.app.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.good.app.base.BaseViewModel
import com.good.app.bean.AppVersionBean
import com.good.app.net.NetRepository

class LoginViewModel: BaseViewModel() {

    var isShowLoginButton = MutableLiveData<Boolean>()
    var username = MutableLiveData<String>()
    var password = MutableLiveData<String>()

    var loginEvent = MutableLiveData<String>()
    var forgotEvent = MutableLiveData<String>()
    var registEvent = MutableLiveData<String>()
    val checkSuccEvent = MutableLiveData<AppVersionBean>()
    val checkCanlEvent = MutableLiveData<Any>()
    val netRepository = NetRepository()

    init {
        isShowLoginButton.value = false
        check("com.gumids.msygj","70")
    }

    fun login(){
        loginEvent.postValue("loginEvent")
    }

    fun goForgotPwd(){
        forgotEvent.postValue("forgotEvent")
    }

    fun goRegister(){
        registEvent.postValue("registEvent")
    }

    //检查更新
    fun check( packageName:String, versionCode:String){
        netRepository.check(packageName,versionCode)
            .subscribe(
                { result ->
                    checkSuccEvent.setValue(result)
                }, { throwable ->
                    checkCanlEvent.setValue("cancel")
                })
    }


}