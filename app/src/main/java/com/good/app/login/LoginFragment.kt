package com.good.app.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.good.app.base.BaseFragment
import com.good.app.databinding.ActivityLoginBinding

class LoginFragment: BaseFragment() {

    lateinit var mBinding: ActivityLoginBinding
    lateinit var mViewModel: LoginViewModel
    companion object{
        fun getInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = ActivityLoginBinding.inflate(inflater,container,false)
        mViewModel = ViewModelProvider(this).get(LoginViewModel()::class.java)
        mBinding?.let {
            it.viewModel = mViewModel
            it.setLifecycleOwner(this)
        }
        setupListener()
        return mBinding.root
    }
    fun setupListener(){
        mViewModel.loginEvent.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context,mViewModel.loginEvent.value,Toast.LENGTH_LONG).show()
        })

        mViewModel.forgotEvent.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context,mViewModel.forgotEvent.value,Toast.LENGTH_LONG).show()
        })

        mViewModel.registEvent.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context,mViewModel.registEvent.value,Toast.LENGTH_LONG).show()
        })
    }


}