package com.good.app.model

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.good.app.base.BaseFragment
import com.good.app.databinding.ActivityModelBinding

class ModelFragment: BaseFragment() {

    lateinit var mBinding: ActivityModelBinding
    lateinit var mViewModel: ModelViewModel
    companion object{
        fun getInstance(): ModelFragment {
            return ModelFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = ActivityModelBinding.inflate(inflater,container,false)
        mViewModel = ViewModelProvider(this).get(ModelViewModel()::class.java)
        mBinding?.let {
            it.viewModel = mViewModel
            it.setLifecycleOwner(this)
        }
        setupListener()
        return mBinding.root
    }
    fun setupListener(){

    }


}