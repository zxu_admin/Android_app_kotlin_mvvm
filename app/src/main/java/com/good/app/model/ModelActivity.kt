package com.good.app.model

import com.good.app.R
import com.good.app.barutil.StatusBarUtil
import com.good.app.barutil.SystemBarManager
import com.good.app.base.BaseActivity
import com.good.app.base.BaseFragment

class ModelActivity : BaseActivity() {
    override fun getFragment(): BaseFragment {
        return ModelFragment.getInstance()
    }
    override fun statusBar() {
        StatusBarUtil.setStatusBarMode(this, true, R.color.c_ffffff)
        SystemBarManager.set(this)
    }
}
