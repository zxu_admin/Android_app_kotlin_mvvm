package com.good.app.guide

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.good.app.BR
import com.good.app.R
import com.good.app.base.BaseFragment
import com.good.app.databinding.ActivityGuideBinding
import java.util.*

class GuideFragment: BaseFragment() {

    lateinit var mBinding: ActivityGuideBinding
    lateinit var mViewModel: GuideViewModel
    private val imageArray =
        arrayOf<Int>(R.mipmap.guide_01, R.mipmap.guide_02, R.mipmap.guide_03)

    companion object{
        fun getInstance(): GuideFragment {
            return GuideFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = ActivityGuideBinding.inflate(inflater,container,false)
        mViewModel = ViewModelProvider(this).get(GuideViewModel()::class.java)
        mBinding?.let {
            it.viewModel = mViewModel
            it.setLifecycleOwner(this)
        }
        setupIndicator()
        setupViewpager()
        return mBinding.root
    }
    private fun setupIndicator() {
        mBinding.indicator.setLength(3)
        mBinding.indicator.setSelected(0)
    }

    private fun setupViewpager() {

        val guideAdapter = activity?.let {
            GuideAdapter(
                it, Arrays.asList(*imageArray), BR.guide_item, R.layout.guide_item_view,
                layoutInflater
            )
        }
        mBinding.vpGuide.setAdapter(guideAdapter)
        mBinding.vpGuide.setOffscreenPageLimit(imageArray.size)
        mBinding.vpGuide.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                mBinding.indicator.setSelected(position)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }


}