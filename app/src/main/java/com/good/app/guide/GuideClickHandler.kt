package com.good.app.guide

import android.view.View

interface GuideClickHandler {
    fun onGoClick(view: View)
}