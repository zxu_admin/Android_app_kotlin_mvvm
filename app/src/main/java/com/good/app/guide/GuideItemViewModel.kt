package com.good.app.guide

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GuideItemViewModel(image: Int?) : ViewModel() {

    private val image: MutableLiveData<Int>

    fun getImage(): LiveData<Int> {
        return image
    }

    init {
        this.image = MutableLiveData()
        this.image.value = image
    }


}