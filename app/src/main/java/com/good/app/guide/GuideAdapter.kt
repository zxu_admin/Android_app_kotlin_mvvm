package com.good.app.guide

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.viewpager.widget.PagerAdapter
import com.good.app.databinding.GuideItemViewBinding
import com.good.app.login.LoginActivity
import com.good.app.main.MainActivity
import com.good.app.util.AcUtils

class GuideAdapter(
    private val mActivity: Activity,
    private val imageList: List<Int>,
    private val variableId: Int,
    private val layoutId: Int,
    private val layoutInflater: LayoutInflater
) :
    PagerAdapter(),GuideClickHandler{

    private var iposition:Int = 0

    override fun onGoClick(view: View) {
        AcUtils.launchActivity(
            mActivity,
            if (isLogin) MainActivity::class.java else LoginActivity::class.java, null
        )
        mActivity.finish()
    }

    /**
     * 获取SP中的用户的登录信息
     *
     * @return true:用户已经登录 false:用户没有登录
     */
    val isLogin: Boolean
        get() = false

    override fun getCount(): Int {
        return imageList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        val binding = `object` as ViewDataBinding
        return view === binding.root
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding =
            DataBindingUtil.inflate<GuideItemViewBinding>(layoutInflater, layoutId, container, true)
        val guideItemViewModel = GuideItemViewModel(imageList[position])
        binding.setVariable(variableId, guideItemViewModel)
        if (position == imageList.size - 1) {
            binding.setClick(this)
        }
        return binding
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val binding = `object` as ViewDataBinding
        container.removeView(binding.root)
    }
}