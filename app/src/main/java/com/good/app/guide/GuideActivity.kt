package com.good.app.guide

import com.good.app.R
import com.good.app.barutil.StatusBarUtil
import com.good.app.barutil.SystemBarManager
import com.good.app.base.BaseActivity
import com.good.app.base.BaseFragment

class GuideActivity : BaseActivity() {
    override fun getFragment(): BaseFragment {
        return GuideFragment.getInstance()
    }
    override fun statusBar() {
        StatusBarUtil.setStatusBarMode(this, true, R.color.c_ffffff)
        SystemBarManager.set(this)
    }
}
