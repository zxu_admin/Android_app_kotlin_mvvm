package com.good.app.util

interface SpTagConst {
    companion object {
        // 用户信息的昵称
        val NICKNAME = "nickname"
        // 用户id
        val USER_ID = "user_id"
        // 用户是否实名制
        val CERTIFICATION = "certification"
        // 是否设置支付密码
        val HAS_PAY_PASSWORD = "has_pay_password"
        val USER_RECOMMAND_URL = "user_recommand_url"
        val IS_AGENT = "is_agent"

        // 保存用户头像
        val AVATAR = "avatar"
        // 用户名
        val USERNAME = "username"

        // 用户手机号
        val PHONE = "phone"

        val NAME = "name"

        val USER_GRADE = "user_grade"

        val TOKEN = "token"
        val LEVEL = "level"
    }
}