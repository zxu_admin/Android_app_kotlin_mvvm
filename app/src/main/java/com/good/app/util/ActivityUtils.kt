package com.good.app.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

class ActivityUtils {

    companion object {
        fun replaceFragmentInActivity(
            fragmentManager: FragmentManager,
            fragment: Fragment, frameId: Int
        ) {
            val transaction = fragmentManager.beginTransaction()
            transaction.replace(frameId, fragment)
            transaction.commit()
        }
    }
}