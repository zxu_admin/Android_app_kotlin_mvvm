package com.good.app.util

import android.app.Activity
import android.content.Context
import android.content.Intent

object AcUtils {

    fun launchActivity(context: Context?, activity: Class<out Activity>, msg: IntentMsg?) {
        if (context == null) {
            return
        }

        val intent = Intent(context, activity)

        if (msg != null) {
            intent.putExtra(IntentMsg.MSG, msg)
        }

        context.startActivity(intent)
    }

}