package com.good.app.base

import android.content.Intent
import androidx.fragment.app.Fragment
import com.good.app.login.LoginActivity

open class BaseFragment: Fragment() {

    fun restartApp() {
        if (activity != null) {
            activity!!.finish()
            val intent = Intent(context, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
    }
}