package com.good.app.net

class ApiException : Exception {
    var code: String? = null
    var displayMessage: String? = null

    constructor(code: String, displayMessage: String) {
        this.code = code
        this.displayMessage = displayMessage
    }

    constructor(code: String, message: String, displayMessage: String) : super(message) {
        this.code = code
        this.displayMessage = displayMessage
    }
}