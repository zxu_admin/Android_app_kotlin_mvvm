package com.good.app.net

class Response<T> {
    var code: String? = null
    var msg: String? = null
    var data: T? = null
}