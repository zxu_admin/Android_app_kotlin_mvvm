package com.good.app.net

import com.good.app.bean.AppVersionBean
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface NetService {

    /***
     * 检查更新
     */
    @GET("/data/api/version/check")
    abstract fun check(@Query("type") type: String, @Query("packageName") packageName: String, @Query("versionCode") versionCode: String): Observable<Response<AppVersionBean>>
}