package com.good.app.spalsh

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.good.app.base.BaseFragment
import com.good.app.databinding.ActivitySplashBinding
import com.good.app.guide.GuideActivity
import com.good.app.login.LoginActivity
import com.good.app.util.AcUtils
import com.good.app.util.SpUtils
import java.util.*

class SplashFragment: BaseFragment() {
    lateinit var mBinding: ActivitySplashBinding
    lateinit var mViewModel: SplashViewModel
    var isShowGuide = false
    var timer = Timer()

    companion object{
        fun getInstance(): SplashFragment {
            return SplashFragment()
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = ActivitySplashBinding.inflate(inflater,container,false)
        mViewModel = ViewModelProvider(this).get(SplashViewModel()::class.java)
        mBinding?.let {
            it.viewModel = mViewModel
            it.setLifecycleOwner(this)
        }
        isShowGuide = SpUtils.get(activity,SpUtils.IS_SHOW_GUIDE,false) as Boolean
        if (!activity!!.isTaskRoot()) {
            activity!!.finish()
        }
        timer.schedule(object : TimerTask() {
            override fun run() {
                timer.cancel()

                if (isShowGuide) {
                    AcUtils.launchActivity(context, LoginActivity::class.java, null)
                } else {
                    AcUtils.launchActivity(context, GuideActivity::class.java, null)
                    activity?.let { SpUtils.put(it, SpUtils.IS_SHOW_GUIDE, true) }
                }
                activity!!.finish()
            }
        }, 3000)

        //setupListener()
        return mBinding.root
    }

    fun setupListener(){

    }


}