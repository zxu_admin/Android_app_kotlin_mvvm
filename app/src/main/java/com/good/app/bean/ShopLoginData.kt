package com.good.app.bean

data class ShopLoginData(
    private var access_token: String? = null,
            var refresh_token: String? = null,
                    var uid: String? = null,
                            var data: List<ShopLoginItemData>? = null,
                                    var level: String? = null,
                                            var nickName: String? = null,
                                                    var name: String? = null,
                                                            var mobile: String? = null,
                                                                    var pic: String? = null,
                                                                            var token_type: String? =
                                                                                null,
                                                                                        var expires_in: Int? =
                                                                                            null,
                                                                                                    var userId: String? =
                                                                                                        null,
                                                                                                                var groupid: String? =
                                                                                                                    null,
                                                                                                                            var robotId: String? =
                                                                                                                                null) {
}